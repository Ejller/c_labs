#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "mem.h"

size_t heap_size;
mem* start_heap;

void* init_new_mem(void* start_address, size_t initial_size){
    size_t size_with_header = initial_size + sizeof(mem);
    mem * tmp = mmap(start_address, size_with_header, PROT_READ | PROT_WRITE, MAP_PRIVATE + MAP_ANONYMOUS, -1, 0);
    heap_size+= size_with_header;
    tmp->capacity = initial_size;
    tmp->is_free = true;
    tmp->next = NULL;
    return start_heap = tmp;
}

void* get_start_address( mem* current_mem ){
    return ((void*)current_mem)+ sizeof(mem);
}


void* get_next_address( mem* last_mem ){
    return get_start_address(last_mem)+last_mem->capacity;
}
mem* find_prev(mem* entry){
    if (entry==start_heap) return NULL;
    for(mem *tmp = start_heap; tmp->next!=NULL; tmp=tmp->next){
        if(tmp->next==entry) return tmp;
    }
    return NULL;
}

bool try_merge_mem(mem *first_mem, mem *second_mem){
    if(get_next_address(first_mem)==(void*)second_mem){
        first_mem->next=second_mem->next;
        first_mem->capacity+=sizeof(mem)+second_mem->capacity;
        return true;
    }
    return false;
}

bool try_split_mem(mem *fit_mem, size_t query){
    if(fit_mem->capacity>query+sizeof(mem)+BLOCK_MIN_SIZE){
        mem* new_mem = (mem*)(get_start_address(fit_mem)+query);
        new_mem->capacity=fit_mem->capacity-query-sizeof(mem);
        new_mem->next=fit_mem->next;
        new_mem->is_free = fit_mem->is_free;
        fit_mem->next=new_mem;
        fit_mem->capacity=query;
        return true;
    }
    return false;
}


void* heap_init( size_t initial_size ){
    return init_new_mem(HEAP_START, initial_size);
}

void  _free( void* start_address ){
    mem *start_address_with_header =(mem *)(start_address-sizeof(mem));
    start_address_with_header->is_free=true;
    if(find_prev(start_address_with_header)!=NULL&& find_prev(start_address_with_header)->is_free ){
        mem * prev = find_prev(start_address_with_header);
        if(try_merge_mem( prev, start_address_with_header))
            start_address_with_header = prev;
    }
    if(start_address_with_header->next->is_free &&start_address_with_header->next!=NULL)
        try_merge_mem(start_address_with_header, start_address_with_header->next);
}

void* _malloc( size_t query ){
    mem *fit_block;
    for (fit_block=start_heap; fit_block!=NULL; fit_block = fit_block->next){
        if(fit_block->capacity<query||!fit_block->is_free){
            continue;
        }
        break;
    }
    if (fit_block==NULL) {
        size_t expand_size = 0.5*heap_size<query?query:0.5*heap_size;
        for (fit_block=start_heap;fit_block->next!=NULL;){
            fit_block=fit_block->next;
        }
        fit_block->next = init_new_mem(get_next_address(fit_block), expand_size);
    }
    try_split_mem(fit_block, query);
    fit_block->is_free = false;
    return get_start_address(fit_block);
}

void* _realloc(void* mem_address, size_t new_size){
    mem * realloc_mem =(mem *)(mem_address-sizeof(mem));
    if (realloc_mem->capacity==new_size) return mem_address;
    if(realloc_mem->capacity<new_size){
        if(realloc_mem->next!=NULL&&realloc_mem->next->is_free) try_merge_mem(realloc_mem, realloc_mem->next);
        if(realloc_mem->capacity<new_size){
            mem * prev_mem = find_prev(realloc_mem);
            if (prev_mem!=NULL&&prev_mem->is_free){
                if (new_size<=realloc_mem->capacity+sizeof(mem)+prev_mem->capacity){
                    if (try_merge_mem(prev_mem, realloc_mem)) {
                        memcpy(get_start_address(prev_mem), get_start_address(realloc_mem), realloc_mem->capacity);
                        realloc_mem = prev_mem;
                        realloc_mem->is_free = false;
                        if(try_split_mem(realloc_mem, new_size)) realloc_mem->next->is_free=true;
                    }
                }
            }
        }
        if(realloc_mem->capacity<new_size){
            void *tmp = _malloc(new_size);
            memcpy(tmp, get_start_address(realloc_mem), realloc_mem->capacity);
            _free(get_start_address(realloc_mem));
            return tmp;
        }
        return get_start_address(realloc_mem);
    } else {
        if (try_split_mem(realloc_mem, new_size)) realloc_mem->next->is_free=true;
        return get_start_address(realloc_mem);
    }
}