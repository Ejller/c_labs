#ifndef BMP_H
#define BMP_H

#include <stdint.h>
//#pragma pack(push, 1)

typedef struct pixel_t {
    unsigned char b, g, r;
} pixel_t;

//#pragma pack(pop)
typedef struct image_t {
    uint64_t width, height;
    pixel_t *data;
} image_t;

typedef enum {
    WRITE_OK = 0,
    WRITE_ERROR = 1
} write_error_code_t;

typedef enum {
    OK = 0,
    INVALID_SIGNATURE,
    INVALID_RESERVED,
    INVALID_HEADER,
    INVALID_OFFSET,
    CORRUPTED
} read_error_code_t;

#pragma pack(push, 1)

typedef struct bmp_header_t {
    uint16_t bfType;
    uint32_t bfileSize;
    uint16_t bfReserved;
    uint16_t bfReserved_d;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} bmp_header_t;

#pragma pack(pop)

read_error_code_t read_header(FILE *, bmp_header_t *);
read_error_code_t read_pixels(FILE *, uint64_t, pixel_t *data, uint64_t, uint64_t);
read_error_code_t read_bmp(FILE *, image_t *);
void rotate_left(image_t *);
void rotate_right(image_t *);
void rotate_apr(image_t *img);
write_error_code_t write_image(FILE *, const image_t *const);
#endif