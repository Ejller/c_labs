#include <stdio.h>
#include <stdlib.h>
#include "bmp.h"


int main() {
    int er;
    image_t *img;
    FILE *in = fopen("test.bmp", "r+");
    FILE *out = fopen("result.bmp", "w");
    if (in == NULL) {
        printf("Source file not found");
        return 0;
    }
    if (out == NULL) {
        printf("Target file not found");
        return 0;
    }

    img = (image_t *) malloc(sizeof(image_t));
    img->width = 0;
    img->height = 0;
    img->data = NULL;
    er = read_bmp(in, img);
    switch (er) {
        case 1:
            printf("INVALID SIGNATURE: %d \n", er);
            return 0;
        case 2:
            printf("INVALID RESERVED: %d \n", er);
            return 0;
        case 3:
            printf("INVALID HEADER: %d \n", er);
            return 0;
        case 4:
            printf("INVALID OFFSET PIXELS:%d \n", er);
            return 0;
        case 5:
            printf("FILE CORRUPTED: %d \n", er);
            return 0;
    }

    rotate_right(img);
    er = write_image(out, img);
    if (er)
        printf("Error rotate right\n");
    printf("OK");
    return 0;

}