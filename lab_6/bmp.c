#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "bmp.h"

read_error_code_t read_header(FILE *const file, bmp_header_t *const header) {
    int detect = fread(header, 1, sizeof(bmp_header_t), file);
    if (header->bfType != 0x4D42)
        return INVALID_SIGNATURE;
    if (header->bfReserved != 0 || header->bfReserved_d != 0)
        return INVALID_RESERVED;
    if (detect != sizeof(bmp_header_t))
        return INVALID_HEADER;
    if (header->bOffBits < 54)
        return INVALID_OFFSET;
    return OK;
}

read_error_code_t read_pixels(FILE *const f, const uint64_t off, pixel_t *data, const uint64_t w, const uint64_t h) {
    uint64_t i;
    uint64_t over = w * 3 % 4 == 0 ? 0 : 4 - (w * 3 % 4);
    fseek(f, off, SEEK_SET);
    for (i = 0; i < h; i++) {
        if (fread(data + i * w, 3, w, f) != w) {
            return CORRUPTED;
        }
        fseek(f, over, SEEK_CUR);
    }
    return OK;
}

read_error_code_t read_bmp(FILE *const f, image_t *const img) {
    int h, w, off;
    read_error_code_t error = 0;
    bmp_header_t *header = malloc(sizeof(bmp_header_t));
    error = read_header(f, header);
    if (error) {
        return error;
    }
    h = header->biHeight;
    w = header->biWidth;
    off = header->bOffBits;
    img->data = malloc(h * w * 3);
    img->height = h;
    img->width = w;
    error = read_pixels(f, off, img->data, w, h);
    return error;
}

void rotate_left(image_t *const img) {
    uint64_t i, j;
    uint64_t w = img->width;
    uint64_t h = img->height;
    pixel_t *new_pix = malloc(sizeof(pixel_t) * w * h);
    for (i = 0; i < w; i++) {
        for (j = 0; j < h; j++) {
            *(new_pix + i * h + j) = *(img->data + (h - j - 1) * w + i);
        }
    }
    img->width = h;
    img->height = w;
    free(img->data);
    img->data = new_pix;
}

void rotate_right(image_t *const img) {
    uint64_t i, j;
    uint64_t w = img->width;
    uint64_t h = img->height;
    pixel_t *new_pix = malloc(sizeof(pixel_t) * w * h);
    for (i = 0; i < w; i++) {
        for (j = 0; j < h; j++) {
            *(new_pix + i * h + j) = *(img->data + j * w + (w - i - 1));
        }
    }
    img->width = h;
    img->height = w;
    free(img->data);
    img->data = new_pix;
}

write_error_code_t write_image(FILE *const f, image_t const *const img) {
    uint64_t i, w, h;
    uint64_t over, i_size, f_size;
    bmp_header_t header;
    const pixel_t *pixels;
    w = img->width;
    h = img->height;
    over = w % 4 == 0 ? 0 : 4 - (w * 3 % 4);
    i_size = (w * 3 + over) * h;
    f_size = sizeof(bmp_header_t) + i_size;
    header.bfType = 0x4D42;
    header.bfileSize = f_size;
    header.bfReserved = 0;
    header.bfReserved_d = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = w;
    header.biHeight = h;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = i_size;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    pixels = img->data;
    if (fwrite(&header, 1, sizeof(header), f) != sizeof(header))
        return WRITE_ERROR;
    for (i = 0; i < h; i++) {
        if (fwrite(pixels + i * w, 1, 3 * w, f) != w * 3)
            return WRITE_ERROR;
        fseek(f, over, SEEK_CUR);
    }
    if (fclose(f) == EOF) {
        return WRITE_ERROR;
    };
    return WRITE_OK;
}