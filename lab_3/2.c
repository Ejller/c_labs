#include <stdio.h>

int is_prime(unsigned long);

int main(int argc, char **argv) {
    unsigned long num;
    int result;

    printf("Enter a number:\n");
    int ret = scanf("%lu", &num);

    if (ret != 1) {
        printf("Error: incorrect input\n");
    } else {
        result = is_prime(num);
        if (result == 1) {
            printf("yes\n");
        } else {
            printf("no\n");
        }
    }
    return 0;
}

int is_prime(unsigned long num) {
    unsigned long i, half;
    half = num / 2;
    for (i = 2; i <= half; i++) {
        if (num % i == 0) {
            return 0;
        }
    }
    return 1;
}
