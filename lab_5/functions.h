#ifndef FUNCTIONS
#define FUNCTIONS
#include "list.h"
#include <stdio.h>

void foreach(llist_t *list, void (*func)(int));
llist_t* map(llist_t *list, int (*func)(int));
void map_mut(llist_t *list, int (*func)(int));
int foldl(llist_t *list, int (*func)(int, int ), int rax);
llist_t *iterate( int s, int n, int (*func)(int));
#endif
