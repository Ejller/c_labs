#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "functions.h"
#include "list.h"

#pragma GCC diagnostic ignored "-Wint-conversion"

static void printWithSpace(int value) {
    printf("%d ", value);
}

static void printWithLine(int value) {
    printf("%d\n", value);
}

static int cube(int value) {
    return value * value * value;
}

static int square(int value) {
    return value * value;
}

static int findMin(int a, int b) {
    return a < b ? a : b;
}

static int findMax(int a, int b) {
    return a > b ? a : b;
}

static int mul2(int a){
    if (a*2>INT_MAX || a*2<INT_MIN) return 0;
    return a*2;
}


static int serialize(llist_t* list, const char * filename){
    llist_t* tmp;
    FILE* file = fopen(filename, "wb");
    if (file == NULL) return 0;
    for (tmp = list; tmp != NULL; tmp = tmp->next){
        fwrite(&(tmp->value), sizeof(int),1, file);
    }
    fclose(file);
    return 1;
}

static llist_t* deserialize(const char* filename){
    int value;
    FILE* file = fopen(filename, "rb");
    llist_t* list = NULL;
    if (file == NULL) return NULL;
    while (1){
        fread(&value,  sizeof(int),1, file);
        if(feof(file)) return list;
        if (list==NULL) {list=list_create(value);}
        else {list = list_add_front(value, &list);}
    }
}


static int save(llist_t *list, const char *filename) {
    FILE *file = fopen(filename, "w");
    if (file == NULL) return 0;
    for (int i = 0; i < list_length(list); i++)
        fprintf(file, "%d ", list_get(i, list));
    fclose(file);
    return 1;
}

static llist_t * load(llist_t *list, const char *filename) {
    int value;
    FILE *file = fopen(filename, "r");
    if (file == NULL) return NULL;
    while (fscanf(file, "%d", &value) != EOF) {

        if (NULL == list) {
            list = list_create(value);
        } else {
            list = list_add_front(value, &list);
        }
    }
    return list;
}


int main() {
    int f[] = {1,2,3,4};
    int l[] = {2,3,4,3};
    l=f;
    int scanValue;
    llist_t *list = NULL;
    llist_t *loadList = NULL;
    while (scanf("%d", &scanValue) != EOF) {
        if (NULL == list) {
            list = list_create(scanValue);
        } else {
            list = list_add_front(scanValue, &list);
        }
    }
    printf("Space: \n");
    foreach(list, printWithSpace);
    printf("\nLine: \n");
    foreach(list, printWithLine);
    llist_t *cubes = map(list, cube);
    printf("Cubes: \n");
    foreach(cubes, printWithSpace);
    llist_t *squares = map(list, square);
    printf("\nSquare: \n");
    foreach(squares, printWithSpace);
    printf("\nMax value: %d\n", foldl(list, findMax, INT_MIN));
    printf("Min value: %d\n", foldl(list, findMin, INT_MAX));
    map_mut(list, abs);
    printf("Module: \n");
    foreach(list, printWithSpace);
    llist_t *squares2 =iterate(2,10,mul2);
    printf("\nDegree 2: \n");
    foreach(squares2,printWithSpace);

    save(list, "1");
    loadList = load(loadList, "1");
    printf("\nLoad file:\n");
    foreach(loadList, printWithSpace);

    serialize(list, "2");
    llist_t * deserializeList = deserialize( "2");
    printf("\nDeserialize file:\n");
    foreach(deserializeList, printWithSpace);

    list_free(squares2);
    list_free(list);
    list_free(squares);
    list_free(cubes);
    return 0;
}




