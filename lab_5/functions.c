#include "list.h"
#include <stdio.h>

#pragma GCC diagnostic ignored "-Wint-conversion"

void foreach(llist_t *list, void (*func)(int)) {
    for (int i = 0; i < list_length(list); i++)
        func(list_get(i, list));
}

llist_t *map(llist_t *list, int (*func)(int)) {
    llist_t *result = list_create(func(list->value));
    for (int i = 1; i < list_length(list); i++) {
        int tmp = func(list_get(i, list));
        list_add_back(tmp, &result);
    }
    return result;
}

void map_mut(llist_t *list, int (*func)(int)) {
    for (int i = 0; i < list_length(list); i++) {
        llist_t *tmp = list_node_at(list, i);
        list_change_value(tmp, func(tmp->value));
    }
}

int foldl(llist_t *list, int (*func)(int, int), int rax) {
    for (int i = 0; i < list_length(list); i++)
        rax = func(rax, list_get(i, list));
    return rax;
}

llist_t *iterate( int s, int n, int (*func)(int)) {
    llist_t *result = list_create(s);
    for (int i = 1; i < n; i++) {
        s = func(s);
        list_add_back(s, &result);
    }
    return result;
}