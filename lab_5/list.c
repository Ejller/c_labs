#include <stdlib.h>
#pragma GCC diagnostic ignored "-Wint-conversion"

typedef struct llist_t {
    int value;
    struct llist_t *next;
} llist_t;


llist_t *list_create(int);
int list_length(llist_t const *);
llist_t *list_node_at(llist_t const *, int);


llist_t *list_node_at(llist_t const *list, const int index) {
    int i;
    for (i = 0; (i < index) && (list->next != NULL); i++) {
        list = list->next;
    }
    if (i == index) {
        return (llist_t *) list;
    } else {
        return NULL;
    }
}

int list_get(const int index, llist_t const *list) {
    if (index > list_length(list)) {
        return NULL;
    } else if ((list = list_node_at(list, index)) != NULL) {
        return list->value;
    } else {
        return NULL;
    }
}

long list_sum(llist_t const *list) {
    long sum = 0;
    while (NULL != list) {
        sum += list->value;
        list = list->next;
    }
    return sum;
}

llist_t *list_add_front(const int number, llist_t **head) {
    llist_t *tmp = malloc(sizeof(llist_t));
    tmp->value = number;
    tmp->next = *head;
    *head = tmp;
    return *head;
}

llist_t *list_add_back(const int number, llist_t **head) {
    llist_t *new = malloc(sizeof(llist_t));
    llist_t *previous = list_node_at(*head, list_length(*head) - 1);
    new->value = number;
    previous->next = new;
    return *head;
}

llist_t *list_create(const int number) {
    llist_t *head = (llist_t *) malloc(sizeof(llist_t));
    head->value = number;
    head->next = NULL;
    return head;
}

int list_length(llist_t const *list) {
    int list_length = 0;
    while (NULL != list) {
        list_length++;
        list = list->next;
    }
    return list_length;
}

void list_change_value(llist_t *list, const int value) {
    list->value=value;
}

void list_free(llist_t *list) {
    llist_t *next;
    if (NULL == list) return;
    for (; list->next; list = next) {
        next = list->next;
        free(list);
    }
    free(list);
}