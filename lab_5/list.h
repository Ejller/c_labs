#ifndef LINKED_LIST
#define LINKED_LIST
#include <stdlib.h>

typedef struct llist_t {
    int value;
    struct llist_t *next;
} llist_t;

void list_change_value(llist_t *list, const int value);
llist_t *list_create(int); //принимает число, возвращает указатель на новый узел связанного списка.
llist_t *list_add_front(int, llist_t **); //принимает число и указатель на указатель на связанный список. Добавляет новый узел в список.
llist_t *list_add_back(int, llist_t **); //принимает число и указатель на указатель на связанный список. Добавляет новый узел в конец списока.
long list_sum(llist_t const *list); //принимает список, возвращает сумму элементов.
int list_length(llist_t const *); //принимает список и вычисляет его длину.
int list_get(int, llist_t const *); //возвращает элемент по индексу или возвращает 0, если индекс находится за пределами списка.
llist_t *list_node_at(llist_t const *, int); //принимает список и индекс, возвращает указатель на struct list, соответствующий узлу в этом индексе. Если индекс слишком велик, возвращает NULL.
void list_free(llist_t *); //отчищает память
#endif
